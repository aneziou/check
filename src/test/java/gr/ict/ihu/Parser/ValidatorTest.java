package gr.ict.ihu.Parser;

import org.junit.Test;

import static org.junit.Assert.*;

public class ValidatorTest {

    @Test
    public void tokenize() {
        String word = "random word";
        Validator validator = new Validator();
        assertEquals("random",validator.tokenize(word));
    }
}