package gr.ict.ihu.Pawns.PawnState;


import gr.ict.ihu.Pawns.Pawn;

public class FreeState implements PawnState{
        private static FreeState instance = new FreeState();

        public FreeState() {}

        public static FreeState instance() {
            return instance;
        }

    @Override
    public void updateState(Pawn pawn) {
            pawn.setPawnCurrentState(this);
    }
}


