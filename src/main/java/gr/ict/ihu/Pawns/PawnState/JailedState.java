package gr.ict.ihu.Pawns.PawnState;

import gr.ict.ihu.Pawns.Pawn;

public class JailedState implements PawnState{
    private static JailedState instance = new JailedState();

    public JailedState() {}

    public static JailedState instance() {
        return instance;
    }

    @Override
    public void updateState(Pawn pawn) {
        pawn.setPawnCurrentState(FreeState.instance());
    }
}
