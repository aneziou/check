package gr.ict.ihu.Pawns.PawnState;

import gr.ict.ihu.Pawns.Pawn;

    public interface PawnState {
    void updateState(Pawn pawn);
}
