package gr.ict.ihu.Pawns.PawnState;

import gr.ict.ihu.Pawns.Pawn;

public class EndedState implements PawnState{
    private static EndedState instance = new EndedState();

    public EndedState() {}

    public static EndedState instance() {
        return instance;
    }

    @Override
    public void updateState(Pawn pawn) {
        pawn.setPawnCurrentState(EndedState.instance());
    }

}
