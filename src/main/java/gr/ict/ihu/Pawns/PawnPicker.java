package gr.ict.ihu.Pawns;

import gr.ict.ihu.Pawns.PawnState.EndedState;
import gr.ict.ihu.Pawns.PawnState.JailedState;
import java.util.List;

public class PawnPicker {


    public String pawnOptions(List<Pawn> pawns){
        if(pawns.size()>1){
            return "Pick a pawn: ";
        }
        return "Picked: Pawn " + (pawns.get(0).getId() + 1);
    }

    public Pawn pawnChooser(int chosenPawn,List<Pawn> pawns){
        Pawn wantedPawn=pawns.get(0);
        if(pawns.size()>1){
            for(Pawn pawn:pawns){
                if(pawn.getId() == chosenPawn) {
                    wantedPawn = pawns.get(chosenPawn);
                    return wantedPawn;
                }
            }
        }
        return wantedPawn;
    }


    public boolean checkIfActive(Pawn pawn,int steps) {
        return (steps == 6 || (!(pawn.getState() instanceof JailedState))) && !(pawn.getState() instanceof EndedState);
    }
}


