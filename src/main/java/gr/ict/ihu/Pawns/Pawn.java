package gr.ict.ihu.Pawns;

import gr.ict.ihu.Pawns.PawnState.PawnState;

public class Pawn {
    private final int id;
    private int currentPosition;
    private final int startingSquare;
    private int steps=0;
    private PawnState state;

    public Pawn(int id, int currentPosition, int startingSquare,PawnState state) {
        this.id = id;
        this.currentPosition = currentPosition;
        this.startingSquare = startingSquare;
        this.state=state;

    }

    public int getId() {
        return id;
    }

    public int getCurrentPosition() {
        return currentPosition;
    }

    public int getStartingSquare() {
        return startingSquare;
    }

    public void setCurrentPosition(int currentPosition) {
        this.currentPosition = currentPosition;
    }

    public void addSteps(int moreSteps){
        steps+=moreSteps;
    }

    public int getSteps() {
        return steps;
    }

    public void setPawnCurrentState(PawnState pawnstate)
    {this.state = pawnstate;}

    public PawnState getState() {
        return state;
    }

    public void update() {
        state.updateState(this);
    }

    public void clearSteps(){
        steps =0;
    }
}
