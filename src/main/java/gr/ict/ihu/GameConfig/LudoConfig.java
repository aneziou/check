package gr.ict.ihu.GameConfig;

import gr.ict.ihu.Model.*;
import gr.ict.ihu.MovePattern.MovePattern;
import gr.ict.ihu.Pawns.Pawn;
import gr.ict.ihu.Pawns.PawnState.JailedState;
import gr.ict.ihu.VictoryCondition.VictoryCondition;


import java.util.ArrayList;
import java.util.List;

public class LudoConfig implements GameConfig {

    private int numberOfSquares;
    private int numberOfDice;
    private int numberOfPawns;
    private VictoryCondition victoryCondition;
    private MovePattern movePattern;
    private int minNumberOfPlayers;
    private int maxNumberOfPlayers;

    @Override
    public List<Status> generatePowerSquares() {
        List<Status> squareList = new ArrayList();
        for (int i = 0; i < numberOfSquares; i++) {
            squareList.add(Status.NONE);
        }
        return squareList;
    }

    @Override
    public List<Player> generatePlayers(int numberOfPlayers) {
        List<Player> players = new ArrayList<>();
        for (int i = 0; i < numberOfPlayers; i++) {
            players.add(new Player("Player" + (i + 1), i,generatePawns(i)));
        }
        return players;
    }

    @Override
    public int getNumberOfDice() {
        return numberOfDice;
    }

    private ArrayList<Pawn> generatePawns(int currentPlayerId) {
        ArrayList<Pawn> playerPawns = new ArrayList<>();
        for (int i = 0; i < numberOfPawns; i++) {
            playerPawns.add(new Pawn(i,(currentPlayerId*10) , (currentPlayerId*10), new JailedState()));
        }
        return playerPawns;
    }

    @Override
    public VictoryCondition getVictoryCondition() {
        return victoryCondition;
    }

    @Override
    public MovePattern getMovementPattern() {
        return movePattern;
    }

    @Override
    public int getMinNumberOfPlayers() {
        return minNumberOfPlayers;
    }

    @Override
    public int getMaxNumberOfPlayers() {
        return maxNumberOfPlayers;
    }
}