package gr.ict.ihu.GameConfig;

import gr.ict.ihu.Model.*;
import gr.ict.ihu.MovePattern.MovePattern;
import gr.ict.ihu.VictoryCondition.VictoryCondition;

import java.util.List;

public interface GameConfig {

    int getNumberOfDice();
    int getMinNumberOfPlayers();
    int getMaxNumberOfPlayers();
    List<Status> generatePowerSquares();
    List<Player> generatePlayers(int numberOfPlayers);
    VictoryCondition getVictoryCondition();
    MovePattern getMovementPattern();
}
