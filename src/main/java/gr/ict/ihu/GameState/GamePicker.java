package gr.ict.ihu.GameState;

import gr.ict.ihu.GSON.GSonLoad;
import gr.ict.ihu.GameConfig.GameConfig;
import gr.ict.ihu.Parser.AvailableOptions;
import gr.ict.ihu.Parser.Validator;
import gr.ict.ihu.Parser.UserInput;
import gr.ict.ihu.UI.Display;


public class GamePicker {

    private final Display display = new Display();
    private final Validator validator = new Validator();
    private UserInput input = new UserInput();

    public String pickGame(){
        String chosenGame;
        do {
            display.showAvailableGames();
            display.showCustomMessage("What to play:");
            chosenGame = validator.tokenize(input.getUserInput());
        } while (!AvailableOptions.contains(chosenGame) && validator.matchToString(chosenGame));
        return chosenGame;
    }

    public State gameInit(GameConfig config,String chosenGame) throws Exception {
        String userChoice;
        int numberOfPlayers;
        State state;
        do{
            display.showGameOption();
            userChoice = validator.tokenize(input.getUserInput());
        }while (!AvailableOptions.contains(userChoice) && validator.matchToString(userChoice));
        switch (userChoice){
            case "NEW":
                do{
                    display.showCustomMessage("Give number of players: ("+config.getMinNumberOfPlayers()+"-"+config.getMaxNumberOfPlayers()+")");
                    numberOfPlayers = validator.matchToInt(input.getUserInput());
                }while (numberOfPlayers < config.getMinNumberOfPlayers() || numberOfPlayers > config.getMaxNumberOfPlayers());
                return new State(config,numberOfPlayers,chosenGame,config.getMovementPattern());
            case "LOAD":
                GSonLoad loader = new GSonLoad();
                state = loader.loadGame(chosenGame);
                return state;
            default:
                throw new Exception("Game not found");
        }
    }
}
