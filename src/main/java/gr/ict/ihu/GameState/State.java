package gr.ict.ihu.GameState;

import gr.ict.ihu.GameConfig.GameConfig;
import gr.ict.ihu.Model.Board;
import gr.ict.ihu.Model.Result;
import gr.ict.ihu.Model.Status;
import gr.ict.ihu.MovePattern.MovePattern;
import gr.ict.ihu.Square.Square;
import gr.ict.ihu.VictoryCondition.VictoryCondition;

public class State {

    private Result result;
    private Board board;
    private final VictoryCondition victoryCondition;
    private final String nameOfTheGame;
    private final MovePattern movePattern;



    public State(GameConfig config,int numberOfPlayers,String nameOfTheGame,MovePattern movePattern) {
        board = new Board(config.generatePlayers(numberOfPlayers), config.generatePowerSquares(),config.getNumberOfDice());
        result = new Result(Status.START, "Game starting...");
        victoryCondition = config.getVictoryCondition();
        this.nameOfTheGame = nameOfTheGame;
        this.movePattern = movePattern;
    }

    public void setGameState(Result gameState, Board board) {
        this.board = board;
        result = gameState;
    }

    public Status getGameStatus() {
        return result.getStatus();
    }

    public String getMessageProgress() {
        return result.getProgressMessage();
    }



    public String ExecutePower(Board board) {
        Square square;
        square = board.getSquareList().get(board.getActivePawn().getCurrentPosition());
        this.board = square.execute(board);
        return board.getActivePlayer().getName() + square.getSquareMessage();
    }

    public boolean checkRoomForPower(Board currentBoard) {
        return currentBoard.getSquareList().get(currentBoard.getActivePawn().getCurrentPosition()).hasPower();
    }

    public Board getBoard() {
        return board;
    }

    public void setGameState(Result result){
        this.result = result;
    }

    public VictoryCondition getVictoryCondition() {
        return victoryCondition;
    }

    public String getNameOfTheGame() {
        return nameOfTheGame;
    }

    public MovePattern getMovePattern() {
        return movePattern;
    }
}
