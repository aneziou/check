package gr.ict.ihu.Square;

import gr.ict.ihu.Model.Board;
import gr.ict.ihu.Pawns.Pawn;
import gr.ict.ihu.Square.SquareState.SquareState;

import java.util.List;

public interface Square {


    Board execute(Board currentBoard);
    int getId();
    boolean hasPower();
    String getSquareMessage();
    List<Pawn> getPawns();
    SquareState getState();
    void addPawn(Pawn pawn);
    void removePawn(Pawn pawn);
    void update();
    void setCurrentState(SquareState state);

}
