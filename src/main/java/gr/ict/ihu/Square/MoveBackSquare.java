package gr.ict.ihu.Square;


import gr.ict.ihu.Model.Board;
import gr.ict.ihu.Pawns.Pawn;
import gr.ict.ihu.Square.SquareState.NormalState;
import gr.ict.ihu.Square.SquareState.SquareState;
import java.util.ArrayList;
import java.util.List;


public class MoveBackSquare implements Square {

    private final int id;
    private final boolean hasPower;
    private final int steps= -3;
    private ArrayList<Pawn> pawns = new ArrayList();
    private SquareState state;

    public MoveBackSquare(int id, boolean hasPower) {
        this.id = id;
        this.hasPower = hasPower;
        this.state = NormalState.instance();
    }


    @Override
    public Board execute(Board currentBoard){
        currentBoard.getActivePawn().setCurrentPosition(currentBoard.getActivePawn().getCurrentPosition()+steps);
        return currentBoard;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public boolean hasPower() {
        return hasPower;
    }

    @Override
    public String getSquareMessage() {
        return " stepped on a trap and now moves " + -1 * steps + " squares back!";
    }

    @Override
    public void addPawn(Pawn pawn) {
        pawns.add(pawn);
    }

    @Override
    public void removePawn(Pawn pawn) {
        pawns.remove(pawn);
    }
    @Override
    public List<Pawn> getPawns() {
        return pawns;
    }

    @Override
    public void update() {
            state.updateState(this);
    }

    @Override
    public SquareState getState() {
        return state;
    }

    @Override
    public void setCurrentState(SquareState state) {
        this.state = state;
    }
}
