package gr.ict.ihu.Square;


import gr.ict.ihu.Model.Board;
import gr.ict.ihu.Pawns.Pawn;
import gr.ict.ihu.Square.SquareState.NormalState;
import gr.ict.ihu.Square.SquareState.SquareState;


import java.util.ArrayList;
import java.util.List;


public class NormalSquare implements Square{
    private int id;
    private boolean hasPower;
    private ArrayList<Pawn> pawns = new ArrayList();
    private SquareState state;

    public NormalSquare(int id, boolean hasPower) {
        this.id = id;
        this.hasPower = hasPower;
        this.state = NormalState.instance();
    }

    @Override
    public Board execute(Board currentBoard){
        throw new UnsupportedOperationException();
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public boolean hasPower() {
        return hasPower;
    }

    @Override
    public String getSquareMessage() {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public void addPawn(Pawn pawn) {
        pawns.add(pawn);
    }

    @Override
    public void removePawn(Pawn pawn) {
        pawns.remove(pawn);
    }
    @Override
    public List<Pawn> getPawns() {
        return pawns;
    }

    public void setCurrentState(SquareState state){
        this.state = state;
    }

    public SquareState getState(){
        return state;
    }

    public void update() {
        state.updateState(this);
    }



}





