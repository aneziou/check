package gr.ict.ihu.Square.SquareState;

import gr.ict.ihu.Square.Square;

public interface SquareState {
    void updateState(Square square);

}
