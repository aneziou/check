package gr.ict.ihu.Square.SquareState;

import gr.ict.ihu.Square.Square;

public class WallState implements SquareState {
    private static WallState instance = new WallState();

    public WallState() {}

    public static WallState instance() {
        return instance;
    }
    @Override
    public void updateState(Square square) {

        if(square.getPawns().size()>1)
        {square.setCurrentState(WallState.instance());
        }
        else{
            square.setCurrentState(BusyState.instance());
        }
    }

    }

