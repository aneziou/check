package gr.ict.ihu.Square.SquareState;
import gr.ict.ihu.Square.Square;

public class BusyState implements SquareState{

    private static BusyState instance = new BusyState();
    public BusyState() {}
    public static BusyState instance() {
        return instance;
    }
    @Override
    public void updateState(Square square) {
        if(square.getPawns().size()==1){
           square.setCurrentState(BusyState.instance());
        }
        else if(square.getPawns().size()<1) {
            square.setCurrentState(NormalState.instance());
        }
        else{
            square.setCurrentState(WallState.instance());
        }
    }
}
