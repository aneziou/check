package gr.ict.ihu.Square.SquareState;


import gr.ict.ihu.Square.Square;

public class NormalState implements SquareState{
    private static NormalState instance = new NormalState();

   public NormalState() {}

    public static NormalState instance() {
        return instance;
    }
    @Override
    public void updateState(Square square) {
       if(square.getPawns().size()==0) {
           square.setCurrentState(NormalState.instance());
       }
       else if (square.getPawns().size()>0){
           square.setCurrentState(BusyState.instance());
       }
    }
}
