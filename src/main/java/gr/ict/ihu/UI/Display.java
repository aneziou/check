package gr.ict.ihu.UI;

public class Display {

    public void showCustomMessage(String message){
        System.out.println(message);
    }


    public void showGameChoices(){
        System.out.println("===================================");
        System.out.println("Roll\tSave\tExit\tChange game");
        System.out.println("===================================");
    }

    public void showAvailableGames(){
        System.out.println("=============");
        System.out.println(" Snake\tLudo");
        System.out.println("=============");
    }

    public void showGameOption() {
        System.out.println("Game Options:" +
                "\n==========================\n  " +
                "NEW GAME \t " +
                "LOAD GAME\n" +
                "==========================");
        System.out.println("What to do: ");
    }
}
