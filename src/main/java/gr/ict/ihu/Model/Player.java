package gr.ict.ihu.Model;

import gr.ict.ihu.Pawns.Pawn;
import java.util.ArrayList;
import java.util.List;

public class Player {

    private final int id;
    private final String name;
    private ArrayList<Pawn> pawns;

    public Player(String name, int id, ArrayList<Pawn> pawns) {
        this.name = name;
        this.id = id;
        this.pawns = pawns;
    }
    public String getName() {
        return name;
    }
    public int getId() {
        return id;
    }
    public List<Pawn> getPawns() {
        return pawns;
    }
}