package gr.ict.ihu.Model;

public enum Status {

    START,
    FINISH,
    CONTINUE,
    BACK,
    FORWARD,
    NONE,
    SAVE,
    LOAD,
    CHANGEGAME,
    INVALID,
    EXIT
}
