package gr.ict.ihu.Model;

import gr.ict.ihu.Pawns.Pawn;
import gr.ict.ihu.Square.*;
import gr.ict.ihu.Square.SquareState.NormalState;
import java.util.*;

public class Board {


    private Player activePlayer;
    private List<Player> playerList;
    private List<Square> squareList = new ArrayList<>();
    private Pawn activePawn;
    private final int numberOfDice;



    public Board(List<Player> players,List<Status> powerSquareList,int numberOfDice) {
        SquareFactory sf = new SquareFactory();
        this.playerList = players;
        for (int i = 0; i < powerSquareList.size(); i++) {
            squareList.add(sf.getSquare(i,powerSquareList.get(i)));
            squareList.get(i).setCurrentState(new NormalState());
            }

        activePlayer = playerList.get(0);
        this.numberOfDice = numberOfDice;

    }

    public void changeToNextPlayer() {
            if(activePlayer.getId()+1< playerList.size()){
                activePlayer = playerList.get(activePlayer.getId()+1);
            }
            else{
                activePlayer = playerList.get(0);
            }
    }

    public Player getActivePlayer(){
        return activePlayer;
    }

    public String getNameOfActivePlayer(){
        return activePlayer.getName();
    }

    public Pawn getActivePawn() {
        return activePawn;
    }

    public List<Square> getSquareList() {
        return squareList;
    }

    public void setActivePawn(Pawn activePawn) {
        this.activePawn = activePawn;
    }

    public int getNumberOfDice() {
        return numberOfDice;
    }

}
