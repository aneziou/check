package gr.ict.ihu.GSON;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import gr.ict.ihu.GameConfig.GameConfig;
import gr.ict.ihu.MovePattern.MovePattern;
import gr.ict.ihu.VictoryCondition.VictoryCondition;

import java.io.*;


public class GSONInit {

    public void serializeInit(GameConfig config, String gameName) throws IOException {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(GameConfig.class,new Adapter())
                .registerTypeAdapter(VictoryCondition.class,new Adapter())
                .registerTypeAdapter(MovePattern.class,new Adapter())
                .setPrettyPrinting().create();
        FileWriter fileWriter = new FileWriter(gameName+"Init.json",false);
        gson.toJson(config, GameConfig.class,fileWriter);
        fileWriter.close();
    }

    public GameConfig deserializeInit(String game) throws IOException {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(GameConfig.class, new Adapter())
                .registerTypeAdapter(VictoryCondition.class,new Adapter())
                .registerTypeAdapter(MovePattern.class,new Adapter())
                .setPrettyPrinting().create();
        Reader reader = new FileReader(game+"Init.json");
        GameConfig config = gson.fromJson(reader, GameConfig.class);
        reader.close();
        return config;
    }
}
