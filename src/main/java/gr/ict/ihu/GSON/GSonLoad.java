package gr.ict.ihu.GSON;



import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import gr.ict.ihu.GameState.State;
import gr.ict.ihu.Model.Result;
import gr.ict.ihu.Model.Status;
import gr.ict.ihu.MovePattern.MovePattern;
import gr.ict.ihu.Pawns.PawnState.PawnState;
import gr.ict.ihu.Square.Square;
import gr.ict.ihu.Square.SquareState.SquareState;
import gr.ict.ihu.VictoryCondition.VictoryCondition;


import java.io.*;


public class GSonLoad {

    public State loadGame(String game) throws IOException {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Square.class, new Adapter())
                .registerTypeAdapter(PawnState.class,new Adapter())
                .registerTypeAdapter(SquareState.class,new Adapter())
                .registerTypeAdapter(VictoryCondition.class,new Adapter())
                .registerTypeAdapter(MovePattern.class,new Adapter())
                .setPrettyPrinting().create();
        Reader reader = new FileReader(game+".json");
        State state = gson.fromJson(reader, State.class);
        reader.close();
        state.setGameState(new Result(Status.LOAD,"Game loading..."));
        return state;
    }
}
