package gr.ict.ihu.GSON;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import gr.ict.ihu.GameState.State;
import gr.ict.ihu.MovePattern.MovePattern;
import gr.ict.ihu.Pawns.PawnState.PawnState;
import gr.ict.ihu.Square.Square;
import gr.ict.ihu.Square.SquareState.SquareState;
import gr.ict.ihu.VictoryCondition.VictoryCondition;

import java.io.FileWriter;
import java.io.IOException;


public class GSonSave {

    public String saveGame(State state, String gameName) throws IOException {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Square.class,new Adapter())
                .registerTypeAdapter(PawnState.class,new Adapter())
                .registerTypeAdapter(SquareState.class,new Adapter())
                .registerTypeAdapter(VictoryCondition.class,new Adapter())
                .registerTypeAdapter(MovePattern.class,new Adapter())
                .setPrettyPrinting().create();
        FileWriter fileWriter = new FileWriter(gameName+".json",false);
        gson.toJson(state,State.class,fileWriter);
        fileWriter.close();
        return "Game saved....";
    }
}
