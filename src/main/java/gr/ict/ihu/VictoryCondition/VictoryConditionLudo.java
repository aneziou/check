package gr.ict.ihu.VictoryCondition;

import gr.ict.ihu.Model.Player;
import gr.ict.ihu.Pawns.Pawn;
import gr.ict.ihu.Pawns.PawnState.EndedState;


public class VictoryConditionLudo implements VictoryCondition {

    @Override
    public boolean checkForWinner(Player activePlayer, Pawn activePawn) {
        int counter = 0;
        for (Pawn pawn : activePlayer.getPawns()){
            if(pawn.getState() == EndedState.instance()){
                counter ++;
            }
        }
        if(counter == activePlayer.getPawns().size()){
            return true;
        }
        return false;
    }
}
