package gr.ict.ihu.VictoryCondition;

import gr.ict.ihu.Model.Player;
import gr.ict.ihu.Pawns.Pawn;


public interface VictoryCondition {

    boolean checkForWinner(Player activePlayer, Pawn activePawn);
}
