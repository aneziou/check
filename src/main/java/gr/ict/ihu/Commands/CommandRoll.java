package gr.ict.ihu.Commands;

import gr.ict.ihu.GameState.State;
import gr.ict.ihu.Model.Board;
import gr.ict.ihu.Model.Dice;
import gr.ict.ihu.Model.Result;
import gr.ict.ihu.Model.Status;
import gr.ict.ihu.Parser.UserInput;
import gr.ict.ihu.Parser.Validator;
import gr.ict.ihu.Pawns.Pawn;
import gr.ict.ihu.Pawns.PawnPicker;
import gr.ict.ihu.Square.Square;
import gr.ict.ihu.UI.Display;

public class CommandRoll implements Command {

    private final PawnPicker picker = new PawnPicker();
    private final Display display = new Display();
    private int intChoice =0;
    private String message;
    private Validator validator = new Validator();
    private UserInput input = new UserInput();
    private boolean active;
    private Pawn pawn;
    private Square wantedSquare;
    private Square currentSquare;

    @Override
    public String doCommand(State currentState) {
        int roll = Dice.rollDice(currentState.getBoard().getNumberOfDice());
        Board board = currentState.getBoard();
        if (currentState.getMovePattern().hasMoves(roll, currentState.getBoard().getActivePlayer())) {
            message = picker.pawnOptions(board.getActivePlayer().getPawns());
            display.showCustomMessage(message);
            if (board.getActivePlayer().getPawns().size() > 1) {
                for (Pawn cpawn:board.getActivePlayer().getPawns())
                {
                    display.showCustomMessage(currentState.getMovePattern().showPossibleMoves(roll, cpawn));
                }
                do{
                    display.showCustomMessage("Chose pawn (1-" + (board.getActivePlayer().getPawns().size()) + "): ");
                    intChoice = (validator.matchToInt(validator.tokenize(input.getUserInput())))-1;
                }while(intChoice<0  || intChoice > board.getActivePlayer().getPawns().size()-1 );
            }
            do{
                pawn = picker.pawnChooser(intChoice, board.getActivePlayer().getPawns());
                active  = picker.checkIfActive(pawn,roll);
                if(!active){
                    display.showCustomMessage("Chose pawn (1-" + (board.getActivePlayer().getPawns().size()) + "): ");
                    intChoice = (Integer.parseInt(input.getUserInput())-1);
                }
            }while(!active);
            board.setActivePawn(pawn);
            wantedSquare = currentState.getMovePattern().findWantedSquare(board.getActivePawn(),board.getSquareList(),roll);
            currentSquare = board.getSquareList().get(board.getActivePawn().getCurrentPosition());
            currentState.getMovePattern().changeStates(wantedSquare,currentSquare,pawn);
            message = currentState.getMovePattern().movePawn(wantedSquare,pawn,board.getActivePlayer(),currentSquare);
            currentState.setGameState(new Result(Status.CONTINUE,"While on square " + currentSquare.getId() + " you rolled:" + roll),board);
            if (currentState.checkRoomForPower(board)) {
                message = currentState.ExecutePower(board);
                wantedSquare = board.getSquareList().get(board.getActivePawn().getCurrentPosition());
            }
            currentState.setGameState(new Result(Status.CONTINUE,currentState.getMessageProgress() +"\n"+message),board);
        } else {
            currentState.setGameState(new Result(Status.CONTINUE,"You rolled: "+ roll+". No possible moves for you!"));
        }
        return currentState.getMessageProgress();
    }
}