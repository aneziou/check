package gr.ict.ihu.Commands;

import gr.ict.ihu.GSON.GSonSave;
import gr.ict.ihu.GameState.State;
import gr.ict.ihu.Model.Result;
import gr.ict.ihu.Model.Status;

import java.io.IOException;

public class CommandSave implements Command {


    @Override
    public String doCommand(State currentState) {

        GSonSave saver = new GSonSave();
        try {
            currentState.setGameState(new Result(Status.SAVE,saver.saveGame(currentState, currentState.getNameOfTheGame())));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return currentState.getMessageProgress();
    }
}
