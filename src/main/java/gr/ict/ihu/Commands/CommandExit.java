package gr.ict.ihu.Commands;

import gr.ict.ihu.GameState.State;
import gr.ict.ihu.Model.Result;
import gr.ict.ihu.Model.Status;

public class CommandExit implements Command {
    @Override
    public String doCommand(State currentState) {
        currentState.getBoard().setActivePawn(currentState.getBoard().getActivePlayer().getPawns().get(0));
        currentState.getBoard().getActivePawn().setCurrentPosition(currentState.getBoard().getSquareList().size()-1);
        currentState.setGameState(new Result(Status.EXIT,"Exiting..."));
        return currentState.getMessageProgress();
    }
}
