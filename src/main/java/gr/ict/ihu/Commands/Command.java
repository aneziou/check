package gr.ict.ihu.Commands;

import gr.ict.ihu.GameState.State;

public interface Command {

    String doCommand(State currentState);
}
