package gr.ict.ihu.Commands;

public class CommandFactory {

    public Command generateCommand(String command){
        switch(command){
            case "CHANGE":
                return new CommandChangeGame();
            case "EXIT":
                return new CommandExit();
            case "ROLL":
                return new CommandRoll();
            case "SAVE":
                return new CommandSave();
            default:
                return new CommandInvalid();
        }
    }
}
