package gr.ict.ihu.Commands;

import gr.ict.ihu.GameState.State;
import gr.ict.ihu.Model.Result;
import gr.ict.ihu.Model.Status;

public class CommandInvalid implements Command {

    @Override
    public String doCommand(State currentState) {
        currentState.setGameState(new Result(Status.INVALID, " Command is invalid"));
        return currentState.getMessageProgress();
    }
}
