package gr.ict.ihu.Parser;

import gr.ict.ihu.Commands.Command;
import gr.ict.ihu.Commands.CommandFactory;
import gr.ict.ihu.GSON.GSONInit;
import gr.ict.ihu.GameConfig.GameConfig;
import gr.ict.ihu.GameState.State;

import java.io.IOException;

public class Parse {
    String message;

    public GameConfig initGameConfig(String chosenGame) throws IOException {
        GSONInit init = new GSONInit();
        return init.deserializeInit(chosenGame);
    }

    public String parseCommand(String command,State currentState){
        CommandFactory commandGenerator = new CommandFactory();
        Command commandToCreate;
        commandToCreate = commandGenerator.generateCommand(command);
        message = commandToCreate.doCommand(currentState);
        return message;
    }
}
