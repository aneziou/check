package gr.ict.ihu.Parser;

public enum AvailableCommands {

    ROLL,
    EXIT,
    SAVE,
    CHANGE;

    public static boolean contains(String game){
        for(AvailableCommands aGame:values()){
            if (aGame.name().equalsIgnoreCase(game)){
                return true;
            }
        }
        return false;
    }
}
