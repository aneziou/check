package gr.ict.ihu.Parser;



public class Validator {

    public String tokenize(String input) {
        return input.trim().split(" ")[0].toUpperCase();
    }

    public int matchToInt(String input){
        if(input.matches("[1-9]+")){
            return Integer.parseInt(input);
        }
        return 0;
    }

    public boolean matchToString(String input){
        return input.matches("[a-zA-Z]+");
    }

}
