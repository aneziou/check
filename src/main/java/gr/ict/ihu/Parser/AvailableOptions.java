package gr.ict.ihu.Parser;

public enum AvailableOptions {
    SNAKE,
    LUDO,
    LOAD,
    NEW;

    public static boolean contains(String game){
        for(AvailableOptions aGame:values()){
            if (aGame.name().equalsIgnoreCase(game)){
                return true;
            }
        }
        return false;
    }
}
