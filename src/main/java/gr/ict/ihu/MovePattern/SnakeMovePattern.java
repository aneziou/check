package gr.ict.ihu.MovePattern;


import gr.ict.ihu.Model.*;
import gr.ict.ihu.Pawns.PawnState.FreeState;
import gr.ict.ihu.Pawns.Pawn;
import gr.ict.ihu.Square.Square;

import java.util.List;


public class SnakeMovePattern implements MovePattern {

    @Override
    public Square findWantedSquare(Pawn activePawn, List<Square> allSquares, int steps) {
        if(activePawn.getCurrentPosition() + steps < allSquares.size()){
            return allSquares.get(activePawn.getCurrentPosition() + steps);
        }
        return allSquares.get(0);
    }

    @Override
    public String movePawn(Square wantedSquare,Pawn activePawn,Player activePlayer,Square currentSquare) {
        activePawn.setCurrentPosition(wantedSquare.getId());
        return "Pawn "+(activePawn.getId()+1) + " moved to square: " + wantedSquare.getId();
    }



    @Override
    public boolean hasMoves(int steps, Player activePlayer) {
        for(Pawn pawn:activePlayer.getPawns()){
            if (pawn.getState() instanceof FreeState)
                return true;
        }
        return false;
    }

    @Override
    public String showPossibleMoves(int steps, Pawn pawn) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void changeStates(Square wantedSquare, Square currentSquare, Pawn activePawn) {
        activePawn.setPawnCurrentState(FreeState.instance());
        currentSquare.removePawn(activePawn);
        wantedSquare.addPawn(activePawn);
        activePawn.update();
        currentSquare.update();
    }
}
