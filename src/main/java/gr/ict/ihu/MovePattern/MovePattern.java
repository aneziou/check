package gr.ict.ihu.MovePattern;




import gr.ict.ihu.Pawns.Pawn;
import gr.ict.ihu.Model.Player;
import gr.ict.ihu.Square.Square;

import java.util.List;


public interface MovePattern {

    Square findWantedSquare(Pawn activePawn, List<Square> allSquares, int steps);
    String movePawn(Square wantedSquare,Pawn activePawn,Player activePlayer,Square currentSquare);
    boolean hasMoves(int steps, Player activePlayer);
    String showPossibleMoves(int steps, Pawn pawn);
    void changeStates(Square wantedSquare,Square currentSquare,Pawn activePawn);
}
