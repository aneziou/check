package gr.ict.ihu.MovePattern;


import gr.ict.ihu.Model.*;
import gr.ict.ihu.Pawns.PawnState.EndedState;
import gr.ict.ihu.Pawns.PawnState.FreeState;
import gr.ict.ihu.Pawns.PawnState.JailedState;
import gr.ict.ihu.Pawns.Pawn;
import gr.ict.ihu.Square.Square;
import java.util.List;


public class LudoMovePattern implements MovePattern{

    @Override
    public boolean hasMoves(int steps, Player activePlayer) {
        for (int i=0;i<activePlayer.getPawns().size();i++){
            if ((activePlayer.getPawns().get(i).getState()) instanceof FreeState|| steps == 6)
               return true;
        }
        return false;
    }

    @Override
    public String showPossibleMoves(int steps, Pawn pawn) {
        String message;

        if (pawn.getState() instanceof FreeState && pawn.getSteps()<40) {
            message= ("Pawn" + (pawn.getId() + 1) + " is free, on square:  " + pawn.getCurrentPosition()); }
        else if(pawn.getState() instanceof JailedState)
        {
            if (steps == 6) {
                message=("Pawn " + (pawn.getId() + 1) + " is jailed, but now you can release it!");
            }
            else
            { message= ("Pawn " + (pawn.getId() + 1) + " is jailed"); }}
         else
             message=("Pawn " + (pawn.getId() + 1) + " ended");
        return message;
    }

    @Override
    public String movePawn(Square wantedSquare, Pawn activePawn,Player activePlayer,Square currentSquare) {
        if (activePlayerIsOwner(wantedSquare, activePlayer, activePawn)) {
            activePawn.addSteps(Math.abs(wantedSquare.getId() - activePawn.getCurrentPosition()));
            activePawn.setCurrentPosition(wantedSquare.getId());
            if (activePawn.getSteps() >= 40) {
                activePawn.setPawnCurrentState(EndedState.instance());
                wantedSquare.removePawn(activePawn);
            }
            wantedSquare.addPawn(activePawn);
            wantedSquare.update();
            return activePlayer.getName() + ": " + "Pawn " + (activePawn.getId() + 1) + " moved to square: " + wantedSquare.getId();
        } else {
            if (wantedSquare.getPawns().size()>=2) {
                currentSquare.addPawn(activePawn);
                currentSquare.update();
                return activePlayer.getName() + ": " + "Pawn " + (activePawn.getId() + 1) + " fell into enemy's wall and cant move!";
            } else  {
                activePawn.clearSteps();
                activePawn.setCurrentPosition(activePawn.getStartingSquare());
                wantedSquare.update();
                activePawn.setPawnCurrentState(new JailedState());
                return activePlayer.getName() + ": " + "Pawn " + (activePawn.getId() + 1) + " fell into enemy's pawn and has been sent to starting square!";
            }
        }
    }

    @Override
    public void changeStates(Square wantedSquare,Square currentSquare,Pawn activePawn)
    {
        activePawn.setPawnCurrentState(FreeState.instance());
        currentSquare.removePawn(activePawn);
        activePawn.update();
        currentSquare.update();
    }

    @Override
    public Square findWantedSquare(Pawn activePawn, List<Square> allSquares, int steps)
    {
        Square wantedSquare;
        if(activePawn.getCurrentPosition()+steps <allSquares.size()){
            wantedSquare = allSquares.get(activePawn.getCurrentPosition() + steps);
        }
        else{
            wantedSquare = allSquares.get(activePawn.getCurrentPosition() - allSquares.size() + steps);
        }
        return wantedSquare;
    }


    private boolean activePlayerIsOwner(Square destinationSquare, Player player, Pawn activePawn) {
        if (destinationSquare.getPawns().isEmpty())
            return true;
        for (int i = 0; i < player.getPawns().size(); i++) {
            if (destinationSquare.getPawns().contains(player.getPawns().get(i))) {
                return true;
            }
        }
        return false;
    }
}

