package gr.ict.ihu;


import gr.ict.ihu.GameConfig.GameConfig;
import gr.ict.ihu.GameState.GamePicker;
import gr.ict.ihu.GameState.State;
import gr.ict.ihu.Parser.AvailableCommands;
import gr.ict.ihu.Parser.Parse;
import gr.ict.ihu.Parser.Validator;
import gr.ict.ihu.Parser.UserInput;
import gr.ict.ihu.Model.*;
import gr.ict.ihu.UI.Display;


public class App {
    static String chosenGame;
    static Display display = new Display();
    static UserInput input = new UserInput();
    static String userChoice;
    static GameConfig gameConfig;
    static Board board;
    static String message = "";
    static State state;
    static Validator validator = new Validator();
    static Parse parser = new Parse();
    static GamePicker gamePicker = new GamePicker();

    public static void main(String[] args) throws Exception {
        do{
            chosenGame = gamePicker.pickGame();
            gameConfig = parser.initGameConfig(chosenGame);
            state = gamePicker.gameInit(gameConfig,chosenGame);
            board = state.getBoard();
            display.showCustomMessage("Game Playing: " + chosenGame);
            do {
                do{
                    display.showGameChoices();
                    display.showCustomMessage(board.getNameOfActivePlayer() + " what to do: ");
                    userChoice = validator.tokenize(input.getUserInput());
                }while (!validator.matchToString(userChoice) || !AvailableCommands.contains(userChoice));

                message = parser.parseCommand(userChoice,state);
                display.showCustomMessage(message);
                if(state.getVictoryCondition().checkForWinner(board.getActivePlayer(),board.getActivePawn())){
                    state.setGameState(new Result(Status.FINISH,board.getNameOfActivePlayer() + " has won!"));
                    display.showCustomMessage(state.getMessageProgress());
                }
                if(state.getGameStatus() != Status.INVALID && state.getGameStatus() != Status.SAVE){
                    board.changeToNextPlayer();
                }
            } while ((state.getGameStatus() != Status.FINISH) && (state.getGameStatus() != Status.CHANGEGAME) && (state.getGameStatus() != Status.EXIT));

        }while ((state.getGameStatus() == Status.CHANGEGAME) && (state.getGameStatus() != Status.EXIT));
    }
}
